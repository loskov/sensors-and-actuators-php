<?php
class Database {
  private static $databaseHandle;

  function __construct() {
    $databaseSourceName = DB_DRIVER . ':dbname=' . DB_DATABASE . ';host='
      . DB_HOSTNAME . ';port=' . DB_PORT;
    self::$databaseHandle = new PDO($databaseSourceName, DB_USERNAME,
      DB_PASSWORD);
  }

  static function getDatabaseHandle(): PDO {
    return self::$databaseHandle ?? (new self())::$databaseHandle;
  }
}
