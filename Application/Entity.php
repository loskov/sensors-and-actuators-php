<?php
class Entity {
  public static $tableName;

  public function set(string $propertyName, $value): Entity {
    $this->$propertyName = $value;

    return $this;
  }

  public static function findAll($callback): array {
    $databaseStatement = Database::getDatabaseHandle()->query(
      'SELECT * FROM `' . static::$tableName . '`');

    return array_map($callback, $databaseStatement->fetchAll(PDO::FETCH_ASSOC));
  }

  public static function deleteById(int $id) {
    $databaseStatement = Database::getDatabaseHandle()->prepare(
      'DELETE FROM `' . static::$tableName . '` WHERE `id` = :id');
    $databaseStatement->bindParam(':id', $id);
    $databaseStatement->execute();
  }

  public static function editNameById(int $id, string $name) {
    $databaseStatement = Database::getDatabaseHandle()->prepare(
      'UPDATE `' . static::$tableName . '`' . "\n"
        . 'SET `name` = :name' . "\n"
        . 'WHERE `id` = :id');
    $databaseStatement->bindParam(':id', $id);
    $databaseStatement->bindParam(':name', $name);
    $databaseStatement->execute();
  }
}
