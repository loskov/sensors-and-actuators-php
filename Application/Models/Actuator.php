<?php
final class Actuator extends Entity {
  var $id, $name, $status, $parameter;
  static $tableName = 'actuators';

  function __construct(?int $id, ?string $name, ?bool $status,
    ?float $parameter) {
      $this->id = $id;
      $this->name = $name;
      $this->status = $status;
      $this->parameter = $parameter;
  }

  public static function create() {
    return new self();
  }

  static function findAll($callback = null): array {
    return parent::findAll(function ($Actuator) {
      return new Actuator((int) $Actuator['id'], $Actuator['name'],
        $Actuator['status'] === '1', (float) $Actuator['parameter']);
    });
  }

  function save(): Actuator {
    $status = (int) $this->status;

    if (is_null($this->id)) {
      $databaseStatement = Database::getDatabaseHandle()->prepare(
        'INSERT INTO `' . static::$tableName
          . '` (`name`, `parameter`, `status`)' . "\n"
          . 'VALUES (:name, :parameter, :status)');
      $databaseStatement->bindParam(':name', $this->name);
      $databaseStatement->bindParam(':parameter', $this->parameter);
      $databaseStatement->bindParam(':status', $status);
    } else {
      $databaseStatement = Database::getDatabaseHandle()->prepare(
        'UPDATE `' . static::$tableName . '`' . "\n"
          . 'SET `name` = :name,' . "\n"
          . '`parameter` = :parameter,' . "\n"
          . '`status` = :status' . "\n"
          . 'WHERE `id` = :id');
      $databaseStatement->bindParam(':id', $this->id);
      $databaseStatement->bindParam(':name', $this->name);
      $databaseStatement->bindParam(':parameter', $this->parameter);
      $databaseStatement->bindParam(':status', $status);
    }

    $databaseStatement->execute();

    return $this;
  }

  static function randomizeParameters(): array {
    return array_map(function ($Actuator) {
      return $Actuator->set('parameter', rand(1, 100))
        ->set('status', (bool) rand(0, 1))->save();
    }, self::findAll());
  }

  function editName() {
    if (!is_null($this->id) && !is_null($this->name)) {
      parent::editNameById($this->id, $this->name);
    }
  }

  function editStatus() {
    if (!is_null($this->id) && !is_null($this->status)) {
      $databaseStatement = Database::getDatabaseHandle()->prepare(
        'UPDATE `' . static::$tableName . '`' . "\n"
          . 'SET `status` = :status' . "\n"
          . 'WHERE `id` = :id');
      $databaseStatement->bindParam(':id', $this->id);
      $status = $this->status ? '1' : '0';
      $databaseStatement->bindParam(':status', $status);
      $databaseStatement->execute();
    }
  }

  function editParameter() {
    if (!is_null($this->id) && !is_null($this->parameter)) {
      $databaseStatement = Database::getDatabaseHandle()->prepare(
        'UPDATE `' . static::$tableName . '`' . "\n"
          . 'SET `parameter` = :parameter' . "\n"
          . 'WHERE `id` = :id');
      $databaseStatement->bindParam(':id', $this->id);
      $databaseStatement->bindParam(':parameter', $this->parameter);
      $databaseStatement->execute();
    }
  }

  function delete() {
    if (!is_null($this->id)) {
      parent::deleteById($this->id);
    }
  }
}
