<?php
final class Sensor extends Entity {
  var $id, $name, $value, $dateTime;
  static $tableName = 'sensors';

  function __construct(?int $id, ?string $name = null, ?float $value = null,
    ?string $dateTime = null) {
      $this->id = $id;
      $this->name = $name;
      $this->value = $value;
      $this->dateTime = $dateTime;
  }

  public static function create() {
    return new self();
  }

  static function findAll($callback = null): array {
    return parent::findAll(function ($sensor) {
      return new Sensor((int) $sensor['id'], $sensor['name'],
        (float) $sensor['value'], $sensor['date_time']);
    });
  }

  function save(): Sensor {
    if (is_null($this->id)) {
      $databaseStatement = Database::getDatabaseHandle()->prepare(
        'INSERT INTO `' . static::$tableName
          . '` (`name`, `value`, `date_time`)' . "\n"
          . 'VALUES (:name, :value, :date_time)');
      $databaseStatement->bindParam(':name', $this->name);
      $databaseStatement->bindParam(':value', $this->value);
      $databaseStatement->bindParam(':date_time', $this->dateTime);
    } else {
      $databaseStatement = Database::getDatabaseHandle()->prepare(
        'UPDATE `' . static::$tableName . '`' . "\n"
          . 'SET `name` = :name,' . "\n"
          . '`value` = :value,' . "\n"
          . '`date_time` = :date_time' . "\n"
          . 'WHERE `id` = :id');
      $databaseStatement->bindParam(':id', $this->id);
      $databaseStatement->bindParam(':name', $this->name);
      $databaseStatement->bindParam(':value', $this->value);
      $databaseStatement->bindParam(':date_time', $this->dateTime);
    }

    $databaseStatement->execute();

    return $this;
  }

  static function randomizeValues(): array {
    return array_map(function ($sensor) {
      return $sensor->set('value', rand(1, 100))
        ->set('dateTime', getCurrentDateTime())->save();
    }, self::findAll());
  }

  function editName() {
    if (!is_null($this->id) && !is_null($this->name)) {
      parent::editNameById($this->id, $this->name);
    }
  }

  function editValue() {
    if (!is_null($this->id) && !is_null($this->value)
      && !is_null($this->dateTime)) {
        $databaseStatement = Database::getDatabaseHandle()->prepare(
          'UPDATE `' . static::$tableName . '`' . "\n"
            . 'SET `value` = :value,' . "\n"
            . '`date_time` = :date_time' . "\n"
            . 'WHERE `id` = :id');
        $databaseStatement->bindParam(':id', $this->id);
        $databaseStatement->bindParam(':value', $this->value);
        $databaseStatement->bindParam(':date_time', $this->dateTime);
        $databaseStatement->execute();
    }
  }

  function delete() {
    if (!is_null($this->id)) {
      parent::deleteById($this->id);
    }
  }
}
