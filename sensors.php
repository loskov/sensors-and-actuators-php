<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Датчики</title>
    <link rel="stylesheet" href="assets/css/common.css?<?php echo time();?>" />
    <link rel="stylesheet" href="assets/css/font-awesome-v4.7.0.min.css" />
  </head>
  <body>
    <section>
      <div class="container">
        <div style="position: relative;">
          <div class="page-title">Датчики</div>
          <div style="position: absolute; top: 0; right: 0;">
            <button class="green" onclick="randomizeAllValuesOfSensors();"><i class="fa fa-random"></i> Сделать случайными все значения</button>
          </div>
        </div>
        <div id="sensors"></div>
      </div>
    </section>
    <script src="assets/js/common.js?<?php echo time();?>"></script>
    <script>
    var sensorsElement = document.getElementById('sensors');

    function getSensorsTable(sensors) {
      if (sensors.length == 0) {
        return '<div class="info" style="margin-bottom: 0;">Нет датчиков.</div>';
      }

      var html = '<table class="sensors">'
        + '<tr>'
        + '<th style="width: 1px;">#</th>'
        + '<th style="width: 50%;">Название</th>'
        + '<th style="width: 15%;">Значение</th>'
        + '<th style="width: 35%;">Дата и время изменения значения</th>'
        + '</tr>';

      sensors.forEach(function (sensor, index) {
        html += '<tr>'
          + '<td>' + (index + 1) + '</td>'
          + '<td class="name">' + sensor.name + '</td>'
          + '<td>' + sensor.value + '&nbsp;<button type="button" class="green small" onclick="Sensor.editValue(' + sensor.id + ', ' + sensor.value + ');"><i class="fa fa-pencil"></i></button></td>'
          + '<td>' + formatDateTime(sensor.dateTime) + '</td>'
          + '</tr>';
      });

      html += '</table>';

      return html;
    }

    function randomizeAllValuesOfSensors() {
      Sensor.randomizeAllValues(function (sensors) {
        sensorsElement.innerHTML = getSensorsTable(sensors);
      });
    }

    Sensor.refreshData();
    </script>
  </body>
</html>
