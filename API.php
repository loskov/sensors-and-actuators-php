<?php
declare(strict_types=1);

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
ini_set('error_reporting', (string) E_ALL);
ini_set('date.timezone', 'Europe/Moscow');

define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_PORT', '3306');
define('DB_USERNAME', 'simulation');
define('DB_PASSWORD', 'xgql3SELNpWQSsS7V17x84aerRbdDKXP');
define('DB_DATABASE', 'simulation_db');

function getCurrentDateTime() {
  list($microseconds, $seconds) = explode(' ', microtime());
  $timestamp = ($seconds + date('Z')) . substr($microseconds, 1, -2);

  return DateTime::createFromFormat('U.u', $timestamp)->format('Y-m-d H:i:s.u');
}

include 'Application/Database.php';
include 'Application/Entity.php';
include 'Application/Models/Sensor.php';
include 'Application/Models/Actuator.php';

$route = isset($_GET['route']) ? $_GET['route'] : null;

header('Content-type: application/json');

switch ($route) {
  case 'sensors':
    echo json_encode(Sensor::findAll());

    break;

  case 'sensors.add':
    $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : null;

    if ($name === null || trim($name) === '') {
      http_response_code(400);
      $success = false;
    } else {
      (new Sensor(null, $name, 0, getCurrentDateTime()))->save();
      $success = true;
    }

    echo json_encode(['success' => $success]);

    break;

  case 'sensors.editName':
    $id = isset($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;
    $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : null;

    if ($id === null || $id < 1 || $name === null || trim($name) === '') {
      http_response_code(400);
      $success = false;
    } else {
      (new Sensor($id, $name, null, null))->editName();
      $success = true;
    }

    echo json_encode(['success' => $success]);

    break;

  case 'sensors.editValue':
    $id = isset($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;
    $value = isset($_REQUEST['value']) ? (float) $_REQUEST['value'] : null;

    if ($id === null || $id < 1 || $value === null || $value < 0) {
      http_response_code(400);
      $success = false;
    } else {
      (new Sensor($id, null, $value, getCurrentDateTime()))->editValue();
      $success = true;
    }

    echo json_encode(['success' => $success]);

    break;

  case 'sensors.delete':
    $id = isset($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;

    if ($id === null || $id < 1) {
      http_response_code(400);
      $success = false;
    } else {
      (new Sensor($id, null, null, null))->delete();
      $success = true;
    }

    echo json_encode(['success' => $success]);

    break;

  case 'sensors.randomizeValues':
    echo json_encode(Sensor::randomizeValues());

    break;

  case 'actuators':
    echo json_encode(Actuator::findAll());

    break;

  case 'actuators.add':
    $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : null;

    if ($name === null || trim($name) === '') {
      http_response_code(400);
      $success = false;
    } else {
      (new Actuator(null, $name, false, 0))->save();
      $success = true;
    }

    echo json_encode(['success' => $success]);

    break;

  case 'actuators.editName':
    $id = isset($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;
    $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : null;

    if ($id === null || $id < 1 || $name === null || trim($name) === '') {
      http_response_code(400);
      $success = false;
    } else {
      (new Actuator($id, $name, null, null))->editName();
      $success = true;
    }

    echo json_encode(['success' => $success]);

    break;

  case 'actuators.editStatus':
    $id = isset($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;
    $status = isset($_REQUEST['status']) ? $_REQUEST['status'] : null;

    if ($id === null || $id < 1 || $status !== '0' && $status !== '1') {
      http_response_code(400);
      $success = false;
    } else {
      (new Actuator($id, null, (bool) $status, null))->editStatus();
      $success = true;
    }

    echo json_encode(['success' => $success]);

    break;


  case 'actuators.editParameter':
    $id = isset($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;
    $parameter = isset($_REQUEST['parameter']) ? (float) $_REQUEST['parameter']
      : null;

    if ($id === null || $id < 1 || $parameter === null || $parameter < 0) {
      http_response_code(400);
      $success = false;
    } else {
      (new Actuator($id, null, null, $parameter))->editParameter();
      $success = true;
    }

    echo json_encode(['success' => $success]);

    break;

  case 'actuators.delete':
    $id = isset($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;

    if ($id === null || $id < 1) {
      http_response_code(400);
      $success = false;
    } else {
      (new Actuator($id, null, null, null))->delete();
      $success = true;
    }

    echo json_encode(['success' => $success]);

    break;

  case 'actuators.randomizeParameters':
    echo json_encode(Actuator::randomizeParameters());

    break;

  default:
    echo json_encode(['error' => ['description' => 'Неизвестный маршрут.']]);

    break;
}
