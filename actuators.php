<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Исполнительные механизмы</title>
    <link rel="stylesheet" href="assets/css/common.css?<?php echo time();?>" />
    <link rel="stylesheet" href="assets/css/font-awesome-v4.7.0.min.css" />
  </head>
  <body>
    <section>
      <div class="container">
        <div class="page-title">Исполнительные механизмы</div>
        <div id="timer" class="info" style="margin-bottom: 10px;">Параметры исполнительных механизмов обновляются автоматически.</div>
        <div id="actuators"></div>
      </div>
    </section>
    <script src="assets/js/common.js?<?php echo time();?>"></script>
    <script>
    var actuatorsElement = document.getElementById('actuators');

    function getactuatorsTable(actuators) {
      if (actuators.length == 0) {
        return '<div class="info" style="margin-bottom: 0;">Нет исполнительных механизмов.</div>';
      }

      var html = '<table class="actuators">'
        + '<tr>'
        + '<th style="width: 1px;">#</th>'
        + '<th style="width: 70%;">Название</th>'
        + '<th style="width: 15%;">Статус</th>'
        + '<th style="width: 15%;">Параметр</th>'
        + '</tr>';

      actuators.forEach(function (Actuator, index) {
        html += '<tr>'
          + '<td>' + (index + 1) + '</td>'
          + '<td class="name">' + Actuator.name + '</td>'
          + '<td class="status ' + (Actuator.status ? 'green' : 'red') + '">' + (Actuator.status ? '<span class="circle"></span>Вкл.' : '<span class="circle"></span>Выкл.') + '</td>'
          + '<td>' + Actuator.parameter + '</td>'
          + '</tr>';
      });

      html += '</table>';

      return html;
    }

    Actuator.refreshData();

    var interval = 10, seconds = interval;

    setInterval(function() {
      seconds -= 1;

      if (seconds > 0) {
        timer.innerText = 'Параметры исполнительных механизмов обновляются автоматически. Следующее обновление будет через '
          + formatSeconds(seconds) + '.';
      } else {
        timer.innerText = 'Происходит обновление данных...';
        Actuator.refreshData();
        seconds = interval;
      }
    }, 1000);
    </script>
  </body>
</html>
