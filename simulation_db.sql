-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `actuators`;
CREATE TABLE `actuators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  `parameter` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `actuators` (`id`, `name`, `status`, `parameter`) VALUES
(13,	'ИМ № 1',	0,	41),
(14,	'ИМ № 2',	0,	94),
(15,	'ИМ № 3',	1,	67),
(16,	'ИМ № 4',	0,	35);

DROP TABLE IF EXISTS `sensors`;
CREATE TABLE `sensors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `date_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `sensors` (`id`, `name`, `value`, `date_time`) VALUES
(16,	'Датчик скорости',	36,	'2017-11-23 21:55:06.591765'),
(17,	'Датчик температуры',	46,	'2017-11-23 21:55:06.593412'),
(18,	'Датчик вибрации',	21,	'2017-11-23 21:55:06.594110'),
(19,	'Датчик углекислого газа',	15,	'2017-11-23 21:55:06.594617'),
(20,	'Датчик метана',	68,	'2017-11-23 21:55:06.595111');

-- 2017-11-23 19:18:51
