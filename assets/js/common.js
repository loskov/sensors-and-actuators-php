function createXmlHttpRequest() {
  var xmlHttp;

  try {
    xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
  } catch (e1) {
    try {
      xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
    } catch (e2) {
      xmlHttp = false;
    }
  }

  if (xmlHttp === false && typeof XMLHttpRequest != 'undefined') {
    xmlHttp = new XMLHttpRequest();
  }

  return xmlHttp;
}

function ajax(url, callback) {
  var request = new createXmlHttpRequest();
  request.open('GET', url, true);
  request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  request.timeout = 30000;
  request.ontimeout = function () {
    alert('Ответ не был получен за 30 секунд.');
  }
  request.send();
  request.onreadystatechange = function () {
    if (request.readyState != 4) {
      return;
    }

    try {
      if (request.status >= 200 && request.status < 300
        || request.status == 304) {
          try {
            var data = JSON.parse(request.responseText);
          } catch (e) {
            alert(e.message + ': ' + xmlHttp.responseText);
            return;
          }

          if (callback !== null) {
            callback(data);
          }
      } else if (request.status != 0) {
        alert('Произошла ошибка: неверные данные.');
      }
    } catch (e) {
      // ontimeout
    }
  }
}

function openPage(route) {
  window.open(route, '_blank');
}

function formatDateTime(dateTime) {
  var regularExpression =
    /(\d{4})-(\d{2})-(\d{2})\s{1}(\d{2}):(\d{2}):(\d{2}).(\d{6})$/g;
  var matches = regularExpression.exec(dateTime);

  return matches[3] + '.' + matches[2] + '.' + matches[1] + ' '
    + matches[4] + ':' + matches[5] + ':' + matches[6] + '.' + matches[7];
}

function formatSeconds(seconds) {
  var cases = ['секунду', 'секунды', 'секунд'];
  var keys = [2, 0, 1, 1, 1, 2, 2, 2, 2, 2];

  return seconds + ' ' + cases[seconds % 100 > 4 && seconds % 100 < 20
    ? 2 : keys[seconds % 10]];
}

var Sensor = {
  getAll: function(callback) {
    ajax('API.php?route=sensors', callback);
  },
  randomizeAllValues: function (callback) {
    ajax('API.php?route=sensors.randomizeValues', callback);
  },
  add: function() {
    var name = prompt('Введите название нового датчика.');

    if (name !== null) {
      ajax('API.php?route=sensors.add&name=' + encodeURIComponent(name),
        function () {
          Sensor.refreshData();
      });
    }
  },
  editName: function(id, name) {
    var newName = prompt('Измените название датчика.', name);

    if (newName !== null) {
      ajax('API.php?route=sensors.editName&id=' + id + '&name='
        + encodeURIComponent(newName), function () {
          Sensor.refreshData();
      });
    }
  },
  editValue: function(id, value) {
    var newValue = prompt('Измените значение датчика.', value);

    if (newValue !== null) {
      ajax('API.php?route=sensors.editValue&id=' + id + '&value='
        + encodeURIComponent(newValue), function () {
          Sensor.refreshData();
      });
    }
  },
  delete: function(id) {
    var isConfirmed = confirm('Вы уверены, что хотите удалить этот датчик?'
      + ' Это действие нельзя отменить.');

    if (isConfirmed) {
      ajax('API.php?route=sensors.delete&id=' + id, function () {
        Sensor.refreshData();
      });
    }
  },
  refreshData: function () {
    Sensor.getAll(function (sensors) {
      sensorsElement.innerHTML = getSensorsTable(sensors);
    });
  }
}

var Actuator = {
  getAll: function (callback) {
    ajax('API.php?route=Actuators', callback);
  },
  randomizeAllParameters: function (callback) {
    ajax('API.php?route=Actuators.randomizeParameters', callback);
  },
  add: function() {
    var name = prompt('Введите название нового исполнительного механизма.');

    if (name !== null) {
      ajax('API.php?route=Actuators.add&name=' + encodeURIComponent(name),
        function () {
          Actuator.refreshData();
      });
    }
  },
  editName: function(id, name) {
    var newName = prompt('Измените название исполнительного механизма.', name);

    if (newName !== null) {
      ajax('API.php?route=Actuators.editName&id=' + id + '&name='
        + encodeURIComponent(newName), function () {
          Actuator.refreshData();
      });
    }
  },
  editStatus: function(id, status) {
    var newStatus = prompt('Измените статус исполнительного механизма:'
      + ' 0 — выключен, 1 — включён.', status);

    if (newStatus !== null) {
      ajax('API.php?route=Actuators.editStatus&id=' + id + '&status='
        + encodeURIComponent(newStatus), function () {
          Actuator.refreshData();
      });
    }
  },
  editParameter: function(id, parameter) {
    var newParameter = prompt('Измените параметр исполнительного механизма.',
      parameter);

    if (newParameter !== null) {
      ajax('API.php?route=Actuators.editParameter&id=' + id + '&parameter='
        + encodeURIComponent(newParameter), function () {
          Actuator.refreshData();
      });
    }
  },
  delete: function (id) {
    var isConfirmed = confirm('Вы уверены, что хотите удалить этот'
      + ' исполнительный механизм? Это действие нельзя отменить.');

    if (isConfirmed) {
      ajax('API.php?route=Actuators.delete&id=' + id, function () {
        Actuator.refreshData();
      });
    }
  },
  refreshData: function () {
    Actuator.getAll(function (Actuators) {
      ActuatorsElement.innerHTML = getActuatorsTable(Actuators);
    });
  }
}
