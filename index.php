<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Личный кабинет</title>
    <link rel="stylesheet" href="assets/css/common.css?<?php echo time();?>" />
    <link rel="stylesheet" href="assets/css/font-awesome-v4.7.0.min.css" />
  </head>
  <body>
    <section>
      <div class="container">
        <div class="page-title">Личный кабинет</div>
        <div style="position: relative;">
          <div class="page-subtitle">Датчики</div>
          <div style="position: absolute; top: 0; right: 0;">
            <button class="green small" onclick="Sensor.add()"><i class="fa fa-plus"></i> Добавить</button>
            <button class="blue small" onclick="openPage('sensors.php')"><i class="fa fa-external-link"></i> Открыть в новой вкладке</button>
          </div>
        </div>
        <div id="timer" class="info" style="margin-bottom: 10px;">Значения датчиков обновляются автоматически.</div>
        <div id="sensors"></div>
        <div style="margin-top: 20px;position: relative;">
          <div class="page-subtitle">Исполнительные механизмы</div>
          <div style="position: absolute; top: 0; right: 0;">
            <button class="green small" onclick="Actuator.add()"><i class="fa fa-plus"></i> Добавить</button>
            <button class="green small" onclick="randomizeAllParametersOfActuators()"><i class="fa fa-random"></i> Сделать случайными все параметры</button>
            <button class="blue small" onclick="openPage('actuators.php')"><i class="fa fa-external-link"></i> Открыть в новой вкладке</button>
          </div>
        </div>
        <div id="Actuators"></div>
      </div>
    </section>
    <script src="assets/js/common.js?<?php echo time();?>"></script>
    <script>
    var sensorsElement = document.getElementById('sensors'),
      ActuatorsElement = document.getElementById('Actuators'),
      timerElement = document.getElementById('timer');

    function getSensorsTable(sensors) {
      if (sensors.length == 0) {
        return '<div class="info" style="margin-bottom: 0;">Нет датчиков.</div>';
      }

      var html = '<table class="sensors">'
        + '<tr>'
        + '<th style="width: 1px;">#</th>'
        + '<th style="width: 50%;">Название</th>'
        + '<th style="width: 15%;">Значение</th>'
        + '<th style="width: 35%;">Дата и время изменения значения</th>'
        + '<th>&nbsp;</th>'
        + '</tr>';

      sensors.forEach(function (sensor, index) {
        html += '<tr>'
          + '<td>' + (index + 1) + '</td>'
          + '<td class="name">' + sensor.name + '&nbsp;<button type="button" class="green small" onclick="Sensor.editName(' + sensor.id + ', \'' + sensor.name + '\');"><i class="fa fa-pencil"></i></button></td>'
          + '<td>' + sensor.value + '</td>'
          + '<td>' + formatDateTime(sensor.dateTime) + '</td>'
          + '<td class="actions"><button type="button" class="red small" onclick="Sensor.delete(' + sensor.id + ');"><i class="fa fa-trash"></i></button></td>'
          + '</tr>';
      });

      html += '</table>';

      return html;
    }

    function getActuatorsTable(Actuators) {
      if (Actuators.length == 0) {
        return '<div class="info" style="margin-bottom: 0;">Нет исполнительных механизмов.</div>';
      }

      var html = '<table class="Actuators">'
        + '<tr>'
        + '<th style="width: 1px;">#</th>'
        + '<th style="width: 70%;">Название</th>'
        + '<th style="width: 15%;">Статус</th>'
        + '<th style="width: 15%;">Параметр</th>'
        + '<th>&nbsp;</th>'
        + '</tr>';

      Actuators.forEach(function (Actuator, index) {
        html += '<tr>'
          + '<td>' + (index + 1) + '</td>'
          + '<td class="name">' + Actuator.name + '&nbsp;<button type="button" class="green small" onclick="Actuator.editName(' + Actuator.id + ', \'' + Actuator.name + '\');"><i class="fa fa-pencil"></i></button></td>'
          + '<td class="status ' + (Actuator.status ? 'green' : 'red') + '">' + (Actuator.status ? '<span class="circle"></span>Вкл.' : '<span class="circle"></span>Выкл.') + '&nbsp;<button type="button" class="green small" onclick="Actuator.editStatus(' + Actuator.id + ', ' + (Actuator.status ? 1 : 0) + ');"><i class="fa fa-pencil"></i></button></td>'
          + '<td>' + Actuator.parameter + '&nbsp;<button type="button" class="green small" onclick="Actuator.editParameter(' + Actuator.id + ', ' + Actuator.parameter + ');"><i class="fa fa-pencil"></i></button></td>'
          + '<td class="actions"><button type="button" class="red small" onclick="Actuator.delete(' + Actuator.id + ');"><i class="fa fa-trash"></i></button></td>'
          + '</tr>';
      });

      html += '</table>';

      return html;
    }

    function randomizeAllParametersOfActuators() {
      Actuator.randomizeAllParameters(function (Actuators) {
        ActuatorsElement.innerHTML = getActuatorsTable(Actuators);
      });
    }

    Sensor.refreshData();
    Actuator.refreshData();

    var interval = 10, seconds = interval;

    setInterval(function() {
      seconds -= 1;

      if (seconds > 0) {
        timer.innerText = 'Значения датчиков обновляются автоматически. Следующее обновление будет через '
          + formatSeconds(seconds) + '.';
      } else {
        timer.innerText = 'Происходит обновление данных...';
        Sensor.refreshData();
        seconds = interval;
      }
    }, 1000);
    </script>
  </body>
</html>
